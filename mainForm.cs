﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ImageReJoin
{
    public partial class mainForm : Form
    {
        Thread threadReJoin;
        Thread threadMerge;
        ImageProcess imageprocess;
        string currentPath;
        public mainForm()
        {
            InitializeComponent();
        }


        private void btReJoin_Click(object sender, EventArgs e)
        {
            imageprocess = new ImageProcess(this, tbDir.Text, tbOrder.Text,cbBackup.Checked);
            threadReJoin = new Thread(new ThreadStart(imageprocess.reJoinImage));
            threadReJoin.IsBackground = true;
            threadReJoin.Start();
        }

        //浏览文件夹
        private void btBrowser_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.SelectedPath = currentPath;
            dialog.Description = "请选择文件路径";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                tbDir.Text = dialog.SelectedPath;
                currentPath = dialog.SelectedPath;
            }
        }

        private void btMerge_Click(object sender, EventArgs e)
        {
            imageprocess = new ImageProcess(this, tbDir.Text, tbOrder.Text, cbBackup.Checked);
            threadMerge = new Thread(new ThreadStart(imageprocess.Merge));
            threadMerge.IsBackground = true;
            threadMerge.Start();
        }
    }
}
