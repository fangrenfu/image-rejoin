﻿namespace ImageReJoin
{
    partial class mainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.btReJoin = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbOrder = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.slStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tbDir = new System.Windows.Forms.TextBox();
            this.btBrowser = new System.Windows.Forms.Button();
            this.cbBackup = new System.Windows.Forms.CheckBox();
            this.btMerge = new System.Windows.Forms.Button();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btReJoin
            // 
            this.btReJoin.Location = new System.Drawing.Point(430, 95);
            this.btReJoin.Margin = new System.Windows.Forms.Padding(4);
            this.btReJoin.Name = "btReJoin";
            this.btReJoin.Size = new System.Drawing.Size(93, 40);
            this.btReJoin.TabIndex = 3;
            this.btReJoin.Text = "开始拼接";
            this.btReJoin.UseVisualStyleBackColor = true;
            this.btReJoin.Click += new System.EventHandler(this.btReJoin_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 108);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "重排顺序：";
            // 
            // tbOrder
            // 
            this.tbOrder.Location = new System.Drawing.Point(132, 102);
            this.tbOrder.Margin = new System.Windows.Forms.Padding(4);
            this.tbOrder.Name = "tbOrder";
            this.tbOrder.Size = new System.Drawing.Size(175, 25);
            this.tbOrder.TabIndex = 5;
            this.tbOrder.Text = "9,8,7,6,5,4,3,2,1,0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 46);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "图片目录：";
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusProgress,
            this.slStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 163);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(648, 26);
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusProgress
            // 
            this.statusProgress.Name = "statusProgress";
            this.statusProgress.Size = new System.Drawing.Size(450, 20);
            this.statusProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // slStatus
            // 
            this.slStatus.Name = "slStatus";
            this.slStatus.Size = new System.Drawing.Size(69, 21);
            this.slStatus.Text = "准备就绪";
            // 
            // tbDir
            // 
            this.tbDir.Location = new System.Drawing.Point(132, 41);
            this.tbDir.Margin = new System.Windows.Forms.Padding(4);
            this.tbDir.Name = "tbDir";
            this.tbDir.Size = new System.Drawing.Size(289, 25);
            this.tbDir.TabIndex = 9;
            // 
            // btBrowser
            // 
            this.btBrowser.Location = new System.Drawing.Point(430, 33);
            this.btBrowser.Margin = new System.Windows.Forms.Padding(4);
            this.btBrowser.Name = "btBrowser";
            this.btBrowser.Size = new System.Drawing.Size(205, 40);
            this.btBrowser.TabIndex = 10;
            this.btBrowser.Text = "浏览...";
            this.btBrowser.UseVisualStyleBackColor = true;
            this.btBrowser.Click += new System.EventHandler(this.btBrowser_Click);
            // 
            // cbBackup
            // 
            this.cbBackup.AutoSize = true;
            this.cbBackup.Location = new System.Drawing.Point(320, 105);
            this.cbBackup.Margin = new System.Windows.Forms.Padding(4);
            this.cbBackup.Name = "cbBackup";
            this.cbBackup.Size = new System.Drawing.Size(89, 19);
            this.cbBackup.TabIndex = 11;
            this.cbBackup.Text = "备份原图";
            this.cbBackup.UseVisualStyleBackColor = true;
            // 
            // btMerge
            // 
            this.btMerge.Location = new System.Drawing.Point(542, 95);
            this.btMerge.Margin = new System.Windows.Forms.Padding(4);
            this.btMerge.Name = "btMerge";
            this.btMerge.Size = new System.Drawing.Size(93, 40);
            this.btMerge.TabIndex = 12;
            this.btMerge.Text = "合成一图";
            this.btMerge.UseVisualStyleBackColor = true;
            this.btMerge.Click += new System.EventHandler(this.btMerge_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 189);
            this.Controls.Add(this.btMerge);
            this.Controls.Add(this.cbBackup);
            this.Controls.Add(this.btBrowser);
            this.Controls.Add(this.tbDir);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbOrder);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btReJoin);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "mainForm";
            this.Text = "图像重新拼接";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btReJoin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbOrder;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        public System.Windows.Forms.ToolStripStatusLabel slStatus;
        private System.Windows.Forms.TextBox tbDir;
        private System.Windows.Forms.Button btBrowser;
        public System.Windows.Forms.ToolStripProgressBar statusProgress;
        private System.Windows.Forms.CheckBox cbBackup;
        private System.Windows.Forms.Button btMerge;
    }
}

